# study_buddy
*DESCRIPTION*

We want to create a web application using React and MongoDB for people to manage their study time/ break time better, with the aid of the pomodoro technique. In addition to providing timers for the students to follow the technique, we’re also providing more sections in the app that will help them organize their workload.

*TARGET AUDIENCE*

Our target audience is the students of the age group 15-16 years who are currently studying in high school. When the workload on a student increases exponentially, and it’s essential for them to learn to manage their study hours/ break time better. These heavy workloads continue in their college years. This technique of pomodoro being used here, often helps people manage their time quite effectively.


