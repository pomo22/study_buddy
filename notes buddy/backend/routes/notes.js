const router = require('express').Router();
const mongoose = require('mongoose');
let Note = require('../models/notes.model');

router.route('/').get((req, res) => {
  const user = (req.query.username);
  console.log(req.query.username);
  Note.find({username : user})
    .then(notes => res.json(notes))
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/add').post((req, res) => {
  const _id = req.body._id;
  const title = req.body.title;
  const content = req.body.content;
  const username = req.body.username;

  // const _id = re.body._id;

  const newNote = new Note({_id, title, content, username});


  newNote.save()
    .then(() => res.json('Note added!'))
    .catch(err => res.status(400).json('Error: ' + err));
});


router.route('/:id').delete((req, res) => {
  Note.findByIdAndDelete(req.params.id)
    .then(() => res.json('Note deleted.'))
    .catch(err => res.status(400).json('Error: ' + err));
});

module.exports = router;
