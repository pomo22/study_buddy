const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const noteSchema = new Schema({
  username: {
    type: String,
  },
  title: {
    type: String,
    trim: true,
  },
  content:{
    type: String
  },
  _id:{
    type: String
  }
}, { _id: false });

const User = mongoose.model('Note', noteSchema);

module.exports = User;
