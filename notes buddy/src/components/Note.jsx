import React, {useState} from "react";
import DeleteIcon from '@material-ui/icons/Delete';

function Note(props) {
  function handleClick() {
    props.onDelete(props.id);
  }

  const [strike, isStruck] = useState(false)
  function handleStrike(){
    isStruck(true);
  }

  return (
    <div className="note">
      <h1>{props.title}</h1>
      <p onClick = {handleStrike} style = {strike? {textDecoration: "line-through"} : null}>{props.content}</p>
      <button onClick={handleClick}><DeleteIcon/></button>
    </div>
  );
}

export default Note;

//when the delete button is clicked the onClick event calss a handle click function
//this handleClick function calls the passed in onDelete function using props.ondelte and passes it a id
//this function links back to app.jsx where it does the actual deletion using the id we where passed back
