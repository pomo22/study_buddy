import React, { useState } from "react";
import AddIcon from '@material-ui/icons/Add';
const bson = require('bson');


function CreateArea(props) {
  // console.log(props.username);
  const [note, setNote] = useState({
    title: "",
    content: "",
    username: props.username,
    _id: new bson.ObjectId().toHexString()
  });

  function handleChange(event) {

    const { name, value } = event.target;

    setNote(prevNote => {
      return {
        ...prevNote,
        [name]: value
      };
    });
  }

  function submitNote(event) {
    // console.log(note, "double here");
    props.onAdd(note);
    setNote({
      title: "",
      content: "",
      username: props.username,
      _id: new bson.ObjectId().toHexString()
    });
    event.preventDefault();
  }

  return (
    <div>
      <form>
        <input
          name="title"
          onChange={handleChange}
          value={note.title}
          placeholder="Title"
        />
        <textarea
          name="content"
          onChange={handleChange}
          value={note.content}
          placeholder="Take a note..."
          rows="3"
        />
        <button onClick={submitNote}><AddIcon/></button>
      </form>
    </div>
  );
}

export default CreateArea;
