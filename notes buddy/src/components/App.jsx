import React, { useState, useEffect } from "react";
import Header from "./Header";
// import Footer from "./Footer";
import Note from "./Note";
import CreateArea from "./CreateArea";
import Login from "./Login";
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';


function App() {
  const [user, setUser] = useState("");
  // var user = "";
  const [notes, setNotes] = useState([]);

  useEffect(() => {
    axios.get('http://localhost:5000/notes/', { params: { username: user } })
      .then(response => {
        console.log(response.data);
        setNotes(response.data);
      })
  }, [user])

  function addNote(newNote) {

    console.log(newNote);
    axios.post('http://localhost:5000/notes/add', newNote)
      .then(res => console.log(res.data));

    axios.post('http://localhost:5000/notes/add', newNote)
      .then(res => console.log(res.data));

    setNotes(prevNotes => {
      return [...prevNotes, newNote];
    });

  }


  function deleteNote(id) {
    console.log(id);
    console.log('http://localhost:5000/notes/' + id);
    axios.delete('http://localhost:5000/notes/' + id)
      .then(res => console.log(res.data));

    setNotes(prevNotes => {
      return prevNotes.filter((noteItem) => {
        return noteItem._id !== id;
      });
    });
  }


function addUser(username){
    setUser((prev) => {
      return username;
    });

  }



  return (
    <div>
    <Login
      onSubmit={addUser}
    />


      <CreateArea onAdd={addNote} username={user}/>
      {notes.map((noteItem, index) => {
        return (
          <Note
            //_id = {new mongoose.Types.ObjectId().toHexString()}
            key={index}
            id={noteItem._id} //change stuff here
            title={noteItem.title}
            content={noteItem.content} //stuff for schema
            username= {noteItem.username}
            onDelete={deleteNote}
          />
        )
      })}


    </div>
  );
}

export default App;
