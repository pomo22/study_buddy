import React, {useState} from "react";
import { Button, Modal, Navbar } from 'react-bootstrap';

function Login(props) {
  const [username, setUser] = useState("");
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

function handleChange(event){
    const txt = event.target.value;
    setUser(txt);

  }

function handleSubmit(event){
    const txt = username;
    console.log(txt, "her");
    props.onSubmit(txt);
    setShow(false);
    event.preventDefault();

  }

  return (

    <div className="login">

      <header>
      <h1>Notes Buddy</h1>
      <button className="login-button-1" onClick={handleShow}>
        Login
      </button>
      </header>

      <Modal show={show} onHide={handleClose}>

      <h1>
        <Modal.Header closeButton className="Modal-header">
          <Modal.Title>Login</Modal.Title>
        </Modal.Header>
        </h1>

        <Modal.Body className="Modal-body">
        <h2>Username</h2>
        <input
          name = "username"
          onChange = {handleChange}
          value= {username}
        />
        </Modal.Body>

        <Modal.Footer className="Modal-footer">
          <button  onClick={handleSubmit}>
            Submit
          </button>
        </Modal.Footer>

        </Modal>
    </div>

  );
}
export default Login;
